# tex
Repository contains LaTeX markup files that are used to build my thesis document, and a submodule
of the parent repository at [pivpr](https://github.com/Jwely/pivpr)
Built using [TexStudio](http://texstudio.sourceforge.net/) on a [MikTex](http://miktex.org/) compiler. 
