# thesis-pivpr/matlab
Code for synthesis of PIV data for demonstration of pressure relaxation phenomena. This is the first and oldest code base writen in matlab. The documentation isn't quite as clear as it could be, and the use of proprietary matlab reduces accessibility. The code is saved here for good record keeping, but the final version is implemented in python with numpy, pandas, matplotlib, etc.

Thesis data was taken of an axial wake vortex in the ODU low speed wind tunnel with a stereo particle image velocimetry system. 
This code is built to synthesize this raw cartesian PIV imagery into a 3d velocity vector field in cylindrical coordinates about
the center of the vortex.
