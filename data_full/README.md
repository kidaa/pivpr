The whole dataset is too large to track in a git repo, but it can be worked with
by downloading the zip files from the release assets page and extracting their contents here.